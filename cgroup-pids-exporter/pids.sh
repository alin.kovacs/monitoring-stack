#!/bin/bash

path=/sys/fs/cgroup/pids

# on arch linux disto 'pids' folder are not present
# fallback on path /sys/fs/cgroup
[[ -d $path ]] || path=/sys/fs/cgroup

shopt -s globstar
for cg in ${path}/**/pids.current; do
    pids=`cat ${cg}`
    if [ "$pids" !=  "0" ]; then
        echo "node_pids_current{cgroup=\"${cg}\"} `cat ${cg}`.0"
    fi
done

